import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Heroe} from "../../servicios/heroes.service";
import {Router} from '@angular/router';

@Component({
  selector: 'app-heroe-card',
  templateUrl: './heroe-card.component.html'
})
export class HeroeCardComponent implements OnInit {

   @Input() heroe:Heroe;
   @Input() index:number;

   @Output() heroeSeleccionado:EventEmitter<number>

  constructor(private _router:Router) {
    this.heroeSeleccionado = new EventEmitter()
  }

  ngOnInit() {
  }

  fnVerHeroe(id: number){
    // this.heroeSeleccionado.emit( this.index )
    this._router.navigate( ['/heroe', id] )
  }

}
