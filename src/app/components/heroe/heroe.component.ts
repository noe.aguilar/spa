import { Component } from '@angular/core';
import {HeroesService, Heroe} from '../../servicios/heroes.service';
import {ActivatedRoute} from '@angular/router';
import {Meta} from "@angular/platform-browser";

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: []
})
export class HeroeComponent {
  heroe:any = {}

  constructor( private _heroesService:HeroesService, private _activateRoute:ActivatedRoute, private _meta:Meta ) {
    _activateRoute.params.subscribe( params => {
      this.heroe = this._heroesService.getHeroe(params.id);

      this._meta.addTag({ name: 'description', content: 'Biografia de Aquaman'});
      this._meta.addTag({ name: 'title', content: this.heroe.nombre });


      this._meta.addTag({ name: "og:url", content: "https://noeaf13.000webhostapp.com/SPA/heroe/" + this.heroe.id });
      this._meta.addTag({ name: "og:type", content: "article" });
      this._meta.addTag({ name: "og:title", content: "Aquaman" });
      this._meta.addTag({ name: "og:description", content: this.heroe.descripcion });
      this._meta.addTag({ name: "og:image", content: "https://noeaf13.000webhostapp.com/SPA/assets/img/"+this.heroe.fb+".png"});


      this._meta.addTag({ name: "twitter:card", content: "summary_large_image" });
      this._meta.addTag({ name: "twitter:creator", content: "@NoeAF13" });
      this._meta.addTag({ name: "twitter:site", content: "@NoeAF13" });
      this._meta.addTag({ name: "twitter:title", content: this.heroe.nombre });
      this._meta.addTag({ name: "twitter:description", content: this.heroe.descripcion });
      this._meta.addTag({ name: "twitter:image", content: "https://noeaf13.000webhostapp.com/SPA/assets/img/"+this.heroe.fb+".png"});

    });

  }



}
