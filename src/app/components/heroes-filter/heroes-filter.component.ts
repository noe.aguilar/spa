import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HeroesService, Heroe} from "../../servicios/heroes.service"

@Component({
  selector: 'app-heroes-filter',
  templateUrl: './heroes-filter.component.html'
})
export class HeroesFilterComponent implements OnInit {

  heroes:Heroe[] = []
  busqueda:string = ""

  constructor( private _activateRoute:ActivatedRoute, private _heroesService:HeroesService ) { }

  ngOnInit() {

    this._activateRoute.params.subscribe(params=>{
      this.busqueda = params.txt
      this.heroes = this._heroesService.buscarHeroes(params.txt);
    })

  }

}
