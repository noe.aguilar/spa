import { Component, OnInit } from '@angular/core';
import {HeroesService, Heroe} from '../../servicios/heroes.service';
import {Router} from '@angular/router';
import {Meta} from "@angular/platform-browser";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
})
export class HeroesComponent implements OnInit {

  heroes:Heroe[] = []
  constructor( private _heroesService: HeroesService, private _router:Router,private _meta:Meta ) {
    // this._meta.addTag({ name: 'twitter:card', content: 'summary_large_image' });
    // this._meta.addTag({ name: 'twitter:site', content: '@alligatorio' });
    // this._meta.addTag({ name: 'twitter:title', content: 'Front-end Web Development, Chewed Up' });
    // this._meta.addTag({ name: 'twitter:description', content: 'Learn frontend web development...' });
    // this._meta.addTag({ name: 'twitter:image', content: 'https://alligator.io/images/front-end-cover.png' });
  }

  ngOnInit() {
    this.heroes = this._heroesService.getHeroes();
  }

  // Para redireccionara otra página desde controlador
  fnVerHeroe(id: number){
    this._router.navigate( ['/heroe', id] )
  }

}
